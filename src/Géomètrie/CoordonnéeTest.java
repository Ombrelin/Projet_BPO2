package Géomètrie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CoordonnéeTest {

	@Test
	void ConstructeurNormtest() {
		Coordonnée c1 = new Coordonnée(1,2);
		assertTrue(c1.getX() == 1 && c1.getY() == 2);
	}
	
	@Test
	void ConstructeurParCopieTest() {
		Coordonnée c1 = new Coordonnée(1,2);
		Coordonnée c2 = new Coordonnée(c1);
		assertTrue(c2.getX() == 1 && c2.getY() == 2);
	}
	
	@Test
	void ConstructeurParCopieRefTest() {
		Coordonnée c1 = new Coordonnée(1,2);
		Coordonnée c2 = new Coordonnée(c1);
		c1.setX(2);
		c1.setY(3);
		assertTrue(c2.getX() == 1 && c2.getY() == 2);
	}
	
	@Test
	void setTest() {
		Coordonnée c1 = new Coordonnée(1,2);
		c1.setX(2);
		assertTrue(c1.getX() == 2);
		try {
			c1.setY(-1);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(),"ordonnée invalide");
		}
		assertFalse(c1.getY() == -1);
	}
	
//	@Test
//	void moinsTest() {
//		Coordonnée c1 = new Coordonnée(3,4);
//		Coordonnée c2 = new Coordonnée(1,2);
//		Coordonnée c3 = c1.moins(c2);
//		assertTrue(c3.getX() == 2 && c3.getY() == 2);
//		try {
//			c3 = c2.moins(c1);
//		} catch (Exception e) {
//			assertEquals(e.getMessage(),"calcul impossible");
//		}
//		assertTrue(c3.getX() == 2 && c3.getY() == 2);
//	}
//
//	@Test
//	void plusTest() {
//		Coordonnée c1 = new Coordonnée(3,4);
//		Coordonnée c2 = new Coordonnée(1,2);
//		Coordonnée c3 = c1.plus(c2);
//		assertTrue(c3.getX() == 4 && c3.getY() == 6);
//	}
	
	@Test
	void equalsTest() {
		Coordonnée c1 = new Coordonnée(3,4);
		Coordonnée c2 = new Coordonnée(3,4);
		assertTrue(c1.equals(c2));
		c2.setX(0);
		assertFalse(c1.equals(c2));
	}
	
	@Test
	void equalsObjTest() {
		Coordonnée c1 = new Coordonnée(3,4);
		int i = 2;
		assertFalse(c1.equals(i));
	}
	
//	@Test
//	void equalsXTest() {
//		Coordonnée c1 = new Coordonnée(3,4);
//		Coordonnée c2 = new Coordonnée(3,4);
//		assertTrue(c1.equalsX(c2));
//		assertTrue(c1.equalsY(c2));
//	}
//	
//	@Test
//	void estCompriseTest() {
//		Coordonnée c1 = new Coordonnée(3,4);
//		Coordonnée deb = new Coordonnée(1,6);
//		Coordonnée fin = new Coordonnée(5,1);
//		assertTrue(c1.estComprise(deb,  fin));
//		Coordonnée c2 = new Coordonnée(1,7);
//		assertFalse(c2.estComprise(deb, fin));
//	}
	
	@Test
	void toStringTest() {
		Coordonnée c1 = new Coordonnée(3,4);
		assertTrue(c1.toString().equals("[3,4]"));
	}
	
}
