package G�om�trie;

public class Coordonn�e {
    private int x;  // abscisse
    private int y;  // ordonn�e

    /**
     * constructeur d'une coordon�e avec pour abscisse x et pour ordonn�e y
     * 
     * @param x, abscisse
     * @param y, ordonn�e
     */
    public Coordonn�e(int x, int y)throws IllegalArgumentException{
    	if(x <0 || y<0)
    		throw new IllegalArgumentException ("valeurs invalides");
        this.x = x;
        this.y = y;
    }
    
    /**
     * constructeur d'une coordonn�e par copie
     * 
     * @param c1, la coordonn�e � "recopier"
     */
    public Coordonn�e(Coordonn�e c) {
		this.x = c.x;
		this.y = c.y;
		//this(c.x, c.y);
	}

    /**
     * renvois l'abscisse d'une coordonn�e
     *
     * @return l'abscisse x
     */
    public int getX() {
        return x;
    }

    /**
     * renvois l'ordonn�e d'une coordonn�e
     *
     * @return l'ordonn�e y
     */
    public int getY() {
        return y;
    }

    /**
     * modification de l'absysce
     *
     * @param x, nouvelle abscisse
     */
    public void setX(int x) throws IllegalArgumentException{
    	if(x < 0)
    		throw new IllegalArgumentException("abscisse invalide");
        this.x = x;
    }

    /**
     * modification de l'ordonn�e
     *
     * @param y, nouvelle ordonn�e
     */
    public void setY(int y) throws IllegalArgumentException{
    	if(y < 0)
    		throw new IllegalArgumentException("ordonn�e invalide");
        this.y = y;
    }
    
//    /**
//     * Soustraction de deux coordonn�es
//     * 
//     * @param c, Coordonn�e � soustraire
//     * @return le r�sultat de la soustraction
//     */
//    public Coordonn�e moins(Coordonn�e c) {
//    	if(x-c.getX() < 0 ||  y-c.getY() <0)
//    		throw new IllegalArgumentException("calcul impossible");
//    	return new Coordonn�e(x-c.getX(), y-c.getY());
//    	
//    }
//
//    /**
//     * Addition de deux coodonn�es
//     * 
//     * @param c, Coordonn�e � ajouter (op�rande de droite)
//     * @return le r�sultat de l'addition
//     */
//	public Coordonn�e plus(Coordonn�e c) {
//		return new Coordonn�e(x+c.getX(), y+c.getY());
//	}
	   
    /**
     * Test si deux Coordonn�e sont �gales
     * 
     * @param c, La Coordonn�e � comparer
     * @return true si �gales, sinon false
     */
	public boolean equals(Coordonn�e c) {
		return (x == c.getX() && y == c.getY());
	}

	/**
	 * �quivalent d'equals mais avec un orbjet � comparer
	 * 
	 * @param obj, l'onjet � comparer
	 * @return true si l'objet est une coordonn�e, et que les coordonn�es sont �gales, false sinon
	 */
    //@Override
    public boolean equalsObj(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordonn�e other = (Coordonn�e) obj;
        return this.equals(other);
    }
        
//    /**
//     * Test si les abcisses des deux Coord sont �gales
//     * 
//     * @param c, La Coordonn�e � comparer
//     * @return true si �gal, sinon false
//     */
//	public boolean equalsX(Coordonn�e c) {
//		return (x == c.getX());
//	}
//	
//    /**
//     * Test si les ordonn�es des deux Coord sont �gales
//     * 
//     * @param c, La Coord � comparer
//     * @return true si �gal, sinon false
//     */
//	public boolean equalsY(Coordonn�e c) {
//		return (y == c.getY());
//	}
//	
//    /**
//     * Test si la coordonn�e est dans le cadre
//     * d�fini par les coord d�but (en haut � gauche) et fin (en bas � droite)
//     * 
//     * @param d�part, La Coord de d�part
//     * @param fin, La Coord de fin
//     * @return true si oui, sinon false
//     */
//	public boolean estComprise(Coordonn�e d�but, Coordonn�e fin) {
//		return x >= d�but.getX() && x <= fin.getX() && y <= d�but.getY() && y >= fin.getY();
//	}
	
	/**
	 * affichage d'une coordonn�e
	 * 
	 * @return la string qui "repr�sente" une coordonn�e
	 */
	@Override
	public String toString() {
		return("[" + this.x + "," + this.y + "]");
	}
}
