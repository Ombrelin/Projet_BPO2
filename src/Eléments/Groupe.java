package El�ments;
import java.util.LinkedList;

import Film.InterfaceEl�ment;
import G�om�trie.Coordonn�e;

public class Groupe extends El�ment{
	private LinkedList<InterfaceEl�ment> groupe;

	/**
	 * Constructeur
	 * @param grp LikedList<El�ments> la collection d'�l�ments
	 * @param coord Coordonn�e la coordonn�e du groupe
	 */
	public Groupe(LinkedList<InterfaceEl�ment> grp, Coordonn�e coord) {
		super(coord);
		groupe = new LinkedList<InterfaceEl�ment>(grp);	
	}

	/**
	 * Test si deux �l�ments sont �quivalents
	 * @param e l'�l�ment � comparer
	 * @return True si les �l�ments sont �quivalents  false sinon
	 */
	@Override
	public boolean equals(InterfaceEl�ment e) {
		if (e == null) {
            return false;
        }
        if (getClass() != e.getClass()) {
            return false;
        }
        final Groupe other = (Groupe) e;
        
        if (!this.getCoord().equals(e.getCoord())){
        	return false;
        }
        
        if (this.groupe.size() != other.groupe.size()) {
        	return false;
        }
        for(int i = 0; i < other.groupe.size(); ++i) {
        	if (!this.groupe.contains(other.groupe.get(i))) {
        		return false;
        	}
        }
        return true;
	}

	/**
	 * repr�sentation sous forme de String de l'�l�ment
	 * @return la string repr�sentant l'�l�ment
	 */
	@Override
	public String toString() {
		return null;
	}
	
	/**
	 * permet de tester si l'�l�ment peut �tre plac� dans l'image
	 * @param int largeur de l'image
	 * @param int hauteur de l'image
	 * @return true si l'�l�ment peut �tre plac�, false si l'�l�ment ne peux pas �tre plac�
	 */
	@Override
	public boolean placable(int largeur, int hauteur) {
		for(InterfaceEl�ment e:groupe) {
			if (!e.placable(largeur, hauteur))
				return false;
		}
		return true;
	}
	
	/**
	 * permet de dessiner un l'�l�ment sur une image
	 * @param char[][] image sur lequel on �crit l'�l�ment 
	 */
	@Override
	public void dessiner(char[][] image) {
		for(InterfaceEl�ment e:groupe) {
			e.dessiner(image);
		}
	}
	
	//faire deplacer de groupe
	@Override
	public void d�placer(Coordonn�e c, int largeur, int hauteur) throws IllegalArgumentException{
		int dx = c.getX() -this.getCoord().getX();
		int dy = c.getY() -this.getCoord().getY();
		for(InterfaceEl�ment ie:groupe) {
			ie.d�placer(new Coordonn�e(ie.getCoord().getX()+dx, ie.getCoord().getY()+dy), largeur,hauteur);
		}
	}
	
	
}
