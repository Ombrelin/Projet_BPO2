package El�ments;

import Film.InterfaceEl�ment;
import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

public class Extrait extends El�ment {
	//Coordonn�e de d�but de l'etrait
	private Coordonn�e c1;
	//Coordonn�e de fin
	private Coordonn�e c2;
	//Texte de l'extrait
	private Texte txt;
	
	/**
	 * Constructeur
	 * @param t un texte dont il faut extraire l'extrait
	 * @param c1 coordonn�e du d�but
	 * @param c2 coordonn�e de la fin
	 * @throws IllegalArgumentException si c1 est plus bas que c2 dans le texte
	 */
	
	public Extrait (Texte t, Coordonn�e c1, Coordonn�e c2, Coordonn�e coord) throws IllegalArgumentException {
		super(coord);
		if ((c1.getX() > c2.getX()) || c1.getY() < c2.getY() )
			throw new IllegalArgumentException("Coordonn�es invalides"); 
		txt = new Texte(t, t.getCoord());
		this.c1 = new Coordonn�e(c1);
		this.c2 = new Coordonn�e(c2);
	}
	
	/**
	 * Constructeur par copie
	 * @param e l'extrait � copier
	 */
	public Extrait (Extrait e) {
		super(e.getCoord());
		txt = new Texte(e.getTexte(),e.getTexte().getCoord());
		this.c1 = new Coordonn�e(e.c1);
		this.c2 = new Coordonn�e(e.c2);
	}
	
	public Texte getTexte () {
		return new Texte(txt,txt.getCoord());
	}
	
	/**
	 * Test si deux extraits sont �quivalents
	 * @param e l'extrait � comparer
	 * @return True si les �extraits sont �quivalents  false sinon
	 */
	public boolean equals (InterfaceEl�ment e) {
		if (e == null) {
            return false;
        }
        if (getClass() != e.getClass()) {
            return false;
        }
        final Extrait other = (Extrait) e;
        
        if (!this.getCoord().equals(e.getCoord())){
        	return false;
        }
        
        return txt.equals(other.txt) && c1.equals(other.c1) && c2.equals(other.c2);
	}
	
	/**
	 * Setter pour les coordonnn�es de l'extrait
	 * @param c1
	 * @param c2
	 */
	//V�rifier par exception que c'est bien dans l'image : l'abcisse est > 0 et < au nb de <-- ya pas un pb puisque l� on est pas dans l'image donc on sait pas 
	//quelle taille elle fait ?
	public void setCoord (Coordonn�e c1, Coordonn�e c2) throws IllegalArgumentException {
		if (c1.getX() < 0 || c1.getX() > txt.taille()[0] || c1.getY() < 0 || c1.getY() > txt.taille()[1] ||
			c2.getX() < 0 || c2.getX() > txt.taille()[0] || c2.getY() < 0 || c2.getY() > txt.taille()[1]) {
			throw new IllegalArgumentException("Coordon�es hors du texte");
		}
		if(c1.getX()> c2.getX() ||c1.getY()< c2.getY() )
			throw new IllegalArgumentException("Coordon�es invalides");
		this.c1 = new Coordonn�e(c1);
		this.c2 = new Coordonn�e(c2);
	}
	
	/**
	 * Permet de conn�itre la taille de l'extrait
	 * @return int[] : en case 0, largeur, en case 1, la hauteur
	 */
	public int[] taille(){
		int l = c2.getX() - c1.getX();
		int h = c1.getY() - c2.getY();
		//int[] t = {l, h};
		int[] t = {l+1, h+1};
		return t;
	}
	
	/**
	 * permet de tester si l'�l�ment peut �tre plac� dans l'image
	 * @param int largeur de l'image
	 * @param int hauteur de l'image
	 * @return true si l'�l�ment peut �tre plac�, false si l'�l�ment ne peux pas �tre plac�
	 */
	public boolean placable(int largeur, int hauteur) {
		if (this.getCoord().getX() < 0 || this.getCoord().getX() > largeur || this.getCoord().getY() < 0 || this.getCoord().getY() > hauteur ) {
			return false;
		}
		else if (this.getCoord().getX() + this.taille()[0] > largeur || this.getCoord().getY() - this.taille()[1] < 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * repr�sentation sous forme de String de l'�l�ment
	 * @return la string repr�sentant l'�l�ment
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();
		return s.toString();
	}
	
	//Exception ou plusieurs if ?
	
	/**
	 * permet de dessiner un l'�l�ment sur une image
	 * @param char[][] image sur lequel on �crit l'�l�ment 
	 */
	@Override
	public void dessiner(char[][] image) {
		int z =0;
		for(int i = c1.getY(); i >= c2.getY(); --i ) {
			int y = 0;
			for (int j = c1.getX(); j <= c2.getX();++j) {
				try {
					image[this.getCoord().getX() + y][this.getCoord().getY() - z] = this.getTexte().getLigne(txt.taille()[1]-(i+1)).charAt(j);
					++y;
				} catch (StringIndexOutOfBoundsException e) {
					continue;
				}
			}
			++z;
		}
		
		if(this.isEncadr�()) {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < taille()[0]+2;++i) {
				sb.append(this.getBordure());
			}
			String s = sb.toString();
			Ligne l1 = new Ligne(s, new Coordonn�e(this.getCoord().getX()-1, this.getCoord().getY()+1));
			Ligne l2 = new Ligne(s, new Coordonn�e(this.getCoord().getX()+taille()[0], this.getCoord().getY()- taille()[1]), Direction.OUEST);
			sb = new StringBuilder();
			for(int i = 0; i < taille()[1]+2;++i) {
				sb.append(this.getBordure());
			}
			s = sb.toString();
			Ligne l3 = new Ligne(s, new Coordonn�e(this.getCoord().getX()-1, this.getCoord().getY()+1), Direction.SUD);
			Ligne l4 = new Ligne(s, new Coordonn�e(this.getCoord().getX()+taille()[0]+1, this.getCoord().getY()- taille()[1]), Direction.NORD);
			
			l1.dessiner(image);
			l2.dessiner(image);
			l3.dessiner(image);
			l4.dessiner(image);
		}
	}
	
}
