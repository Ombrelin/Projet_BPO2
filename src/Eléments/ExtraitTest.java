package Eléments;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

import Géomètrie.Coordonnée;
import Géomètrie.Direction;

class ExtraitTest {

	@Test
	void Constructeurtest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(1,1);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e= new Extrait(t, c1, c2, coord);
		//c1.setX(1);
		//c2.setX(1);
		coord.setX(1);
		assertFalse(e.getCoord().equals(coord));
		t.addLigne("aaaa");
		assertFalse(e.getTexte().equals(t));
		assertFalse(e.isEncadré());
	}
	
	@Test
	void ConstructuerCopieTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(1,1);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e= new Extrait(t, c1, c2, coord);
		//coord.setX(3);
		Extrait e1 = new Extrait(e);
		assertTrue(e.getTexte().equals(e1.getTexte()));
		assertTrue(e.getCoord().equals(e1.getCoord()));	
	}
	
	@Test 
	void ConstructeurCopieRefTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(3,3);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(3,3);
		Extrait e= new Extrait(t, c1, c2, coord);
		Extrait e1 = new Extrait(e);
		coord.setX(4);
		e.déplacer(coord, 100,100);
		assertFalse(e.getCoord().equals(e1.getCoord()));
		//ajouter setTexte ?
	}
	
	@Test
	void setCoordTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(1,1);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(1,0);
		Coordonnée c1 = new Coordonnée(0,1);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e1= new Extrait(t, c1, c2, coord);
		Extrait e2= new Extrait(t, c1, c2, coord);
		
		//les coordonnées sont dans le texte et valides
		Coordonnée c4 = new Coordonnée(2,0);
		Coordonnée c3 = new Coordonnée(0,2);
		e1.setCoord(c3, c4);
		assertFalse(e1.equals(e2));
		
		//les coordonnées sont dans le texte mais invalides
		e1.setCoord(c1, c2);
		try {
			e1.setCoord(c4, c3);
		} catch(IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Coordonées invalides");
		}
		assertTrue(e1.equals(e2));
		
		//les coordonnées sont en degors du texte
		e1.setCoord(c1, c2);
		c3.setY(5);
		c4.setX(5);
		try {
			e1.setCoord(c3, c4);
		} catch(IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Coordonées hors du texte");
		}
		assertTrue(e1.equals(e2));
	}
	
	@Test
	void getTexteTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(1,1);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e= new Extrait(t, c1, c2, coord);
		assertTrue(e.getTexte().equals(t));
		t.addLigne("aaaa");
		assertFalse(e.getTexte().equals(t));
	}
	
	@Test
	void equalsTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(10,10);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(3,3);
		Extrait e1= new Extrait(t, c1, c2, coord);
		Extrait e2 = new Extrait(t, c1, c2, coord);
		assertTrue(e1.equals(e2));
		
		e2.déplacer(c, 100, 100);
		assertFalse(e1.equals(e2));
		
		e2.déplacer(coord, 100, 100);
		Coordonnée c3 = new Coordonnée(3,0);
		e2.setCoord(c1, c3);
		assertFalse(e1.equals(e2));
		
		t.addLigne(s3);
		Extrait e3 = new Extrait(t,c1, c2, coord);
		assertFalse(e3.equals(e1));
	}
	
	@Test
	void tailleTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(10,10);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e1= new Extrait(t, c1, c2, coord);
		assertTrue(e1.taille()[0] == 3);
		assertTrue(e1.taille()[1] == 3);
	}
	
	@Test
	void placableTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(10,10);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e1= new Extrait(t, c1, c2, coord);
		assertFalse(e1.placable(100, 100));
		
		coord.setX(3);
		coord.setY(3);
		e1.déplacer(coord, 100, 100);
		assertTrue(e1.placable(100, 100));
		assertFalse(e1.placable(3, 3));
	}
	
	@Test
	void encadrerTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(10,10);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e1= new Extrait(t, c1, c2, coord);
		e1.encadrer('x');
		assertTrue(e1.isEncadré());
	}
	
	void desencadrerTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(10,10);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e1= new Extrait(t, c1, c2, coord);
		e1.encadrer('x');
		e1.desencadrer();
		assertFalse(e1.isEncadré());
	}
	
	@Test
	void getBordureTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonnée c = new Coordonnée(10,10);
		Texte t = new Texte(txt, c);
		Coordonnée c2 = new Coordonnée(2,0);
		Coordonnée c1 = new Coordonnée(0,2);
		Coordonnée coord = new Coordonnée(2,2);
		Extrait e1= new Extrait(t, c1, c2, coord);
		e1.encadrer('x');
		assertTrue(e1.getBordure() == 'x');
	}
}
