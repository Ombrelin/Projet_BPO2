package El�ments;

import Film.InterfaceEl�ment;
import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

public abstract class El�ment implements InterfaceEl�ment{
	//Coordonn�es de l'�l�ment
	private Coordonn�e Coord;
	//Bordure de l'�l�ment
	private char bordure;
	private boolean encadr�;
	
	/**
	 * Constructeur vide
	 * @param coord Coordonn�e � attribuer
	 */
	public El�ment (Coordonn�e coord) {
		this.Coord = new Coordonn�e(coord);
		encadr� = false;
	}
	
	/**
	 * Getter pour les coordonn�es de l'�l�ment
	 * @return Coordonn�e de l'�l�ment
	 */
	@Override
	public Coordonn�e getCoord() {
		return this.Coord;
	}
	
	/**
	 * Setter pour les coordonn�es de l'�l�ment
	 * @param Coordonn�es c la nouvelle coordonn�e
	 */
	@Override
	public void d�placer(Coordonn�e c, int largeur, int hauteur) throws IllegalArgumentException{
		Coordonn�e temp = new Coordonn�e(this.getCoord());
		this.Coord = new Coordonn�e(c);
		if(!this.placable(largeur, hauteur)) {
			this.Coord = new Coordonn�e(temp);
			throw new IllegalArgumentException("nouvelle coordonn�e invalide");
		}
	}
	
	@Override
	//mettre meme verif que l'autre non ?
	public void d�placer(int distance, Direction d) {
		switch(d) {
		case SUD:
			this.getCoord().setY(this.getCoord().getY()-distance);
			break;
		case NORD:
			this.getCoord().setY(this.getCoord().getY()+distance);
			break;
		case EST:
			this.getCoord().setX(this.getCoord().getX()+distance);
			break;
		case OUEST:
			this.getCoord().setX(this.getCoord().getX()-distance);
			break;
		case SUD_OUEST:		
			this.getCoord().setX(this.getCoord().getX()-distance);
			this.getCoord().setY(this.getCoord().getY()-distance);
			break;
		case SUD_EST:
			this.getCoord().setX(this.getCoord().getX()+distance);
			this.getCoord().setY(this.getCoord().getY()-distance);
			break;
		case NORD_OUEST:
			this.getCoord().setX(this.getCoord().getX()-distance);
			this.getCoord().setY(this.getCoord().getY()+distance);
			break;
		case NORD_EST:
			this.getCoord().setX(this.getCoord().getX()+distance);
			this.getCoord().setY(this.getCoord().getY()+distance);
			break;
		}
	}

	
	/**
	 * repr�sentation sous forme de String de l'�l�ment
	 * @return la string repr�sentant l'�l�ment
	 */
	@Override
	public abstract String toString ();
	
	/**
	 * Permet d'encadrer un �l�ment
	 * @param Character c � utiliser pour encadrer
	 */
	@Override
	public void encadrer (char c) {
		this.encadr� = true;
		this.bordure = c;
	}
	
	@Override
	public char getBordure() {
		return bordure;
	}
	
	public boolean isEncadr�() {
		return this.encadr�;
	}
	
	/**
	 * Permet de d�sencadrer un �l�ment	
	 */
	@Override
	public void desencadrer () {
		this.encadr� = false;
	}
	
	/**
	 * permet de tester si l'�l�ment peut �tre plac� dans l'image
	 * @param int largeur de l'image
	 * @param int hauteur de l'image
	 * @return true si l'�l�ment peut �tre plac�, false si l'�l�ment ne peux pas �tre plac�
	 */
	@Override
	public abstract boolean placable(int largeur, int hauteur);
	
	/**
	 * permet de dessiner un l'�l�ment sur une image
	 * @param char[][] image sur lequel on �crit l'�l�ment 
	 */
	@Override
	public abstract void dessiner(char[][] image);
}

