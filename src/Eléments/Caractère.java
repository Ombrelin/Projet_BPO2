package El�ments;

import Film.InterfaceEl�ment;
import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

public class Caract�re extends El�ment {
	//Contenu du caract�re
	private char content;
	
	/**
	 * Constructeur par copie
	 * @param c un Caract�re � copier
	 * @param coord Coordonn�e � attribuer
	 */
	public Caract�re (Caract�re c, Coordonn�e coord) throws IllegalArgumentException{
		super(coord);
		if (coord.getX() < 0 || coord.getY() < 0) {
			throw new IllegalArgumentException("Coordon�e invalide");
		}
		content=c.content;
	}
	
	/**
	 * Constructeur par copie
	 * @param c un Caract�re � copier
	 */
	public Caract�re (Caract�re c) {
		super(c.getCoord());
		content=c.getCarac();
	}
	
	/**
	 * Constructeur
	 * @param c un char
	 * @param coord Coordonn�e � attribuer
	 */
	public Caract�re (char c, Coordonn�e coord) throws IllegalArgumentException {
		super(coord);
		if (coord.getX() < 0 || coord.getY() < 0) {
			throw new IllegalArgumentException("Coordon�e invalide");
		}
		content = c;
	}
	
	/**
	 * Test si deux �l�ments sont equivalents
	 * @param e un �l�ment � comparer
	 * @return True s'ils sont �quivalents, false sinon
	 */
	public boolean equals (InterfaceEl�ment e) {
		if (e == null) {
            return false;
        }
        if (getClass() != e.getClass()) {
            return false;
        }
        final Caract�re other = (Caract�re) e;
        if (!this.getCoord().equals(e.getCoord())){
        	return false;
        }
        return content == other.getCarac();
	}
	
	/**
	 * Test si un caract�re contient est bien le char pass� en param�tre
	 * @param e le char � comparer
	 * @return True s'ils sont �quivalents, false sinon
	 */
	public boolean equalsCarac (char e) {
		return(this.content == e);
	}
	
	/**
	 * Modifie un caract�re
	 * @param e
	 */
	public void setCarac (char e) {
		content = e;
	}
	
	/**
	 * Getter pour le contenu du caract�re
	 * @return
	 */
	public char getCarac () {
		return content;
	}
	
	/**
	 * permet de tester si l'�l�ment peut �tre plac� dans l'image
	 * @param int largeur de l'image
	 * @param int hauteur de l'image
	 * @return true si l'�l�ment peut �tre plac�, false si l'�l�ment ne peux pas �tre plac�
	 */
	public boolean placable(int largeur, int hauteur) {
		if (this.getCoord().getX() < 0 || this.getCoord().getX() > largeur || this.getCoord().getY() < 0 || this.getCoord().getY() > hauteur ) {
			return false;
		}
		return true;
	}
	
	
	/**
	 * renvoi la repr�sentation sous forme de string
	 * @return la string
	 */
	public String toString () {
		StringBuilder s = new StringBuilder();
		s.append(content);
		return s.toString();
	}
	
	/**
	 * permet de dessiner un l'�l�ment sur une image
	 * @param char[][] image sur lequel on �crit l'�l�ment 
	 */
	public void dessiner(char[][] image) {
		image[this.getCoord().getX()][this.getCoord().getY()] = this.getCarac();
		if (this.isEncadr�() == true) {
			Character c = this.getBordure();
			String s = c.toString() + c.toString() + c.toString();
			Ligne l1 = new Ligne(s, new Coordonn�e(this.getCoord().getX()-1, this.getCoord().getY()+1));
			Ligne l2 = new Ligne(s, new Coordonn�e(this.getCoord().getX()+1, this.getCoord().getY()-1), Direction.OUEST);
			l1.dessiner(image);
			l2.dessiner(image);
			l1.setDirection(Direction.SUD);
			l2.setDirection(Direction.NORD);
			l1.dessiner(image);
			l2.dessiner(image);
		}
	}
}
