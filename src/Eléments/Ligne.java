package El�ments;

import Film.InterfaceEl�ment;
import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

public class Ligne extends El�ment {
	//Caract�re de la ligne
	private String ligne;
	//Taille de la ligne
	private int taille;
	//Direction actuelle de la ligne
	private Direction d;
	
	/**
	 * Constructeur d'une ligne d'un m�me caract�re
	 * @param taille taille de la ligne
	 * @param c caract�re � r�p�ter
	 * @param coord Coordonn�e � attribuer
	 */
	public Ligne(String c, Coordonn�e coord) {
		super(coord);
		d = Direction.EST;
		taille= c.length();
		ligne = new String(c);
	}
	
	/**
	 * Constructeur d'une ligne d'un m�me caract�re
	 * @param taille taille de la ligne
	 * @param c caract�re � r�p�ter
	 * @param d la direction � donner
	 * @param coord Coordonn�e � attribuer
	 */
	public Ligne(String c, Coordonn�e coord, Direction d) {
		super(coord);
		this.d = d;
		taille= c.length();
		ligne = new String(c);
	}
	
	/**
	 * Constructeur par copie
	 * @param l ligne � copier
	 */
	public Ligne (Ligne l) {
		super(l.getCoord());
		ligne = l.ligne;
		taille = l.taille;
		d = l.d;
	}
	
	/**
	 * Getter pour le caract�re de la ligne
	 * @return char caract�re
	 */
	public String getCarac() {
		return new String(ligne);
	}
	
	/**
	 * Getter pour la taille de la ligne
	 * @return int taille de la ligne
	 */
	public int getTaille() {
		return taille;
	}
	
	/**
	 * Getter pour la direction de la ligne
	 * @return Direction direction de la ligne
	 */
	public Direction getDirection() {
		return d;
	}
	
	/**
	 * Setter pour la direction de la ligne
	 * @param Direction d la nouvelle direction de la ligne
	 */
	public void setDirection(Direction d) {
		this.d = d;
	}
	
	/**
	 * Setter pour le caract�re de la ligne
	 * @param Caract�re c nouveau caract�re de la ligne
	 */
	public void setCarac(String c) {
		ligne = new String(c);
		taille = c.length();
	}
	
	/**
	 * Test si deux lignes sont �quivalentss
	 * @param e ligne � comparer
	 * @return True si les lignes sont �quivalents  false sinon
	 */
	public boolean equals (InterfaceEl�ment e) {
		if (e == null) {
            return false;
        }
        if (getClass() != e.getClass()) {
            return false;
        }
        if (!this.getCoord().equals(e.getCoord())){
        	return false;
        }
        final Ligne other = (Ligne) e;
        if(this.getCarac().equals(other.getCarac()))
        	 return true;
        else 
        	return false;
	}
	
	/**
	 * repr�sentation sous forme de String de l'�l�ment
	 * @return la string repr�sentant l'�l�ment
	 */
	public String toString () {
		StringBuilder s = new StringBuilder();
		return s.toString();
	}
	
	/**
	 * permet de tester si l'�l�ment peut �tre plac� dans l'image
	 * @param int largeur de l'image
	 * @param int hauteur de l'image
	 * @return true si l'�l�ment peut �tre plac�, false si l'�l�ment ne peux pas �tre plac�
	 */
	public boolean placable(int largeur, int hauteur) {
		if (this.getCoord().getX() < 0 || this.getCoord().getX() > largeur || this.getCoord().getY() < 0 || this.getCoord().getY() > hauteur ) {
			return false;
		}
		if (d.toString().contains("NORD")) {
			if(this.getCoord().getY() + this.taille >hauteur)
				return false;
		}
		if (d.toString().contains("EST") || d.toString().contains("_EST")) {
			if(this.getCoord().getX() + this.taille >largeur)
				return false;
		}
		if (d.toString().contains("SUD")) {
			if(this.getCoord().getY() - this.taille < 0)
				return false;
		}
		if (d.toString().contains("OUEST") || d.toString().contains("_OUEST")) {
			if(this.getCoord().getX() - this.taille < 0)
				return false;
		}
		return true;
	}
	
	/**
	 * permet de dessiner un l'�l�ment sur une image
	 * @param char[][] image sur lequel on �crit l'�l�ment 
	 */
	public void dessiner(char[][] image) {
		image[this.getCoord().getX()][this.getCoord().getY()] = this.getCarac().charAt(0);
		int x = this.getCoord().getX();
		int y = this.getCoord().getY();
		for(int i = 1;i<taille;++i) {
			if (d.toString().contains("NORD")) {
				y+=1;				
			}
			if (d.toString().contains("_EST") || d.toString().equals("EST")) {
				x+=1;
			}
			if (d.toString().contains("SUD")) {
				y-=1;
			}
			if (d.toString().contains("_OUEST") || d.toString().equals("OUEST")) {
				x-=1;
			}
			image[x][y] = this.getCarac().charAt(i);
		}
		if(this.isEncadr�()) {
			StringBuilder sb = new StringBuilder();
			Ligne l1;
			Ligne l2;
			Caract�re c1;
			Caract�re c2;
			int cot� = (int)Math.ceil((getTaille()+2) * Math.sin(45));
			switch(this.getDirection()) {
			case SUD:
				
				for(int i = 0; i < this.getTaille()+2;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+1,this.getCoord().getY()+1),Direction.SUD);
				l2 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-1,this.getCoord().getY()+1),Direction.SUD);
				c1 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX(),this.getCoord().getY()+1));
				c2 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX(),this.getCoord().getY()-getTaille()));
				l1.dessiner(image);
				l2.dessiner(image);
				c1.dessiner(image);
				c2.dessiner(image);
				break;
			case NORD:
				for(int i = 0; i < this.getTaille()+2;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+1,this.getCoord().getY()-1),Direction.NORD);
				l2 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-1,this.getCoord().getY()-1),Direction.NORD);
				c1 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX(),this.getCoord().getY()-1));
				c2 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX(),this.getCoord().getY()+getTaille()));
				l1.dessiner(image);
				l2.dessiner(image);
				c1.dessiner(image);
				c2.dessiner(image);
				break;
			case EST:
				for(int i = 0; i < this.getTaille()+2;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-1,this.getCoord().getY()+1));
				l2 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-1,this.getCoord().getY()-1));
				c1 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX()-1,this.getCoord().getY()));
				c2 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX()+getTaille(),this.getCoord().getY()));
				l1.dessiner(image);
				l2.dessiner(image);
				c1.dessiner(image);
				c2.dessiner(image);
				break;
			case OUEST:
				for(int i = 0; i < this.getTaille()+2;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+1,this.getCoord().getY()+1),Direction.OUEST);
				l2 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+1,this.getCoord().getY()-1),Direction.OUEST);
				c1 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX()+1,this.getCoord().getY()));
				c2 = new Caract�re(getBordure(),new Coordonn�e(this.getCoord().getX()-getTaille(),this.getCoord().getY()));
				l1.dessiner(image);
				l2.dessiner(image);
				c1.dessiner(image);
				c2.dessiner(image);
				break;
			case SUD_OUEST:
				for(int i = 0; i < cot� +1;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+1,this.getCoord().getY()+1), Direction.SUD);
				l1.dessiner(image);
				l1.setDirection(Direction.OUEST);
				l1.dessiner(image);
				l2 =  new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-cot�+1,this.getCoord().getY()-cot�+1), Direction.NORD);
				l2.dessiner(image);
				l2.setDirection(Direction.EST);
				l2.dessiner(image);
				break;
			case SUD_EST:
				for(int i = 0; i < cot�+1;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-1,this.getCoord().getY()+1));
				l1.dessiner(image);
				l1.setDirection(Direction.SUD);
				l1.dessiner(image);
				l2 =  new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+cot�,this.getCoord().getY()-cot�+1), Direction.NORD);
				l2.dessiner(image);
				l2.setDirection(Direction.OUEST);
				l2.dessiner(image);
				break;
			case NORD_OUEST:
				for(int i = 0; i < cot�+1;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+1,this.getCoord().getY()-1),Direction.OUEST);
				l1.dessiner(image);
				l1.setDirection(Direction.NORD);
				l1.dessiner(image);
				l2 =  new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-cot�+1,this.getCoord().getY()+cot�-1), Direction.SUD);
				l2.dessiner(image);
				l2.setDirection(Direction.EST);
				l2.dessiner(image);
				break;
			case NORD_EST:
				for(int i = 0; i < cot�+1;++i) {
					sb.append(getBordure());
				}
				l1 = new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()-1,this.getCoord().getY()-1));
				l1.dessiner(image);
				l1.setDirection(Direction.NORD);
				l1.dessiner(image);
				l2 =  new Ligne(sb.toString(),new Coordonn�e(this.getCoord().getX()+cot�,this.getCoord().getY()+cot�-1), Direction.SUD);
				l2.dessiner(image);
				l2.setDirection(Direction.OUEST);
				l2.dessiner(image);
				break;
			}
		}
	}
}
