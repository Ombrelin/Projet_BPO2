package El�ments;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

class Caract�reTest {

	@Test
	void ConstructeurNormtest() {
		Caract�re c = new Caract�re('a', new Coordonn�e(1,1));
		assertTrue(c.getCarac() == 'a');
		assertTrue(c.getCoord().equals(new Coordonn�e(1,1)));
		assertFalse(c.isEncadr�());
	}
	
	@Test
	void ConstructeurParCopieTest() {
		Caract�re c = new Caract�re('a', new Coordonn�e(1,1));
		assertTrue(c.getCarac() == 'a');
	}
	
	@Test
	void ConstructeurParCopieRefTest() {
		Caract�re c1 = new Caract�re('a', new Coordonn�e(1,1));
		Caract�re c2 = new Caract�re(c1);
		c1.setCarac('e');
		c1.d�placer(new Coordonn�e(2,2),100,100);
		assertFalse(c2.getCarac() == 'e');
		assertTrue(c2.getCoord().equals(new Coordonn�e(1,1)));
	}
	
	@Test
	void getCaracTest() {
		Caract�re c1 = new Caract�re('a', new Coordonn�e(1,1));
		assertTrue(c1.getCarac() == 'a');
	}
	
	@Test
	void getCoordtest() {
		Caract�re c = new Caract�re('a', new Coordonn�e(1,1));
		assertTrue(c.getCoord().equals(new Coordonn�e(1,1)));
	}
	
	@Test
	void equalsTest() {
		Caract�re c1 = new Caract�re('a', new Coordonn�e(1,1));
		Caract�re c2 = new Caract�re('a', new Coordonn�e(1,1));
		assertTrue(c1.equals(c2));		
		Ligne ligne = new Ligne("aaaaaaaaaaaaaa", new Coordonn�e(1,1));
		assertFalse(c1.equals(ligne));
	}
	
	@Test
	void equalsCaracTest() {
		Caract�re c1 = new Caract�re('a', new Coordonn�e(1,1));
		assertTrue(c1.equalsCarac('a'));
	}
	
	@Test
	void setCaracTest() {
		Caract�re c1 = new Caract�re('a', new Coordonn�e(1,1));
		c1.setCarac('e');
		assertTrue(c1.getCarac() == 'e');
	}
	
	@Test
	void toStringTest() {
		Caract�re c1 = new Caract�re('a', new Coordonn�e(1,1));
		assertTrue(c1.toString().equals("a"));
	}
	
	@Test
	void placableTest() {
		Caract�re c1 = new Caract�re('a', new Coordonn�e(2,1));
		assertTrue(c1.placable(3, 3));
		assertFalse(c1.placable(1, 1));
	}
	
	@Test
	void dessinerTest() {
		
	}
	
	@Test 
	void d�placerTest() {
		Coordonn�e coord1 = new Coordonn�e(2,1);
		Caract�re c1 = new Caract�re('a', coord1);
		Coordonn�e coord2 = new Coordonn�e(1,1);
		c1.d�placer(coord2, 100,100);
		assertFalse(c1.getCoord().equals(coord1));
		assertTrue(c1.getCoord().equals(coord2));
	}
	
	@Test
	void encadrerTest() {
		Coordonn�e coord1 = new Coordonn�e(2,1);
		Caract�re c1 = new Caract�re('a', coord1);
		c1.encadrer('x');
		assertTrue(c1.isEncadr�());
	}
	
	void desencadrerTest() {
		Coordonn�e coord1 = new Coordonn�e(2,1);
		Caract�re c1 = new Caract�re('a', coord1);
		c1.encadrer('x');
		c1.desencadrer();
		assertFalse(c1.isEncadr�());
	}
	
	@Test
	void getBordureTest() {
		Coordonn�e coord1 = new Coordonn�e(2,1);
		Caract�re c1 = new Caract�re('a', coord1);
		c1.encadrer('x');
		assertTrue(c1.getBordure() == 'x');
	}

}
