package Eléments;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import Géomètrie.*;

class LigneTest {

	@Test
	void ConstructeurNormLigneTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c);
		assertTrue(l.getCarac().equals("cccccccccc"));
		assertTrue(l.getCoord().equals(new Coordonnée(1,1)));
		assertTrue(l.getTaille() == 10);
		assertTrue(l.getDirection() == Direction.EST);
		assertFalse(l.isEncadré());
	}
	
	@Test
	void ConstructeurDirecLigneTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.OUEST);
		assertTrue(l.getDirection() == Direction.OUEST);
	}
	
	@Test
	void getCaracTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.OUEST);
		assertTrue(l.getCarac().equals("cccccccccc"));
	}
	
	@Test
	void getTailleTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.OUEST);
		assertTrue(l.getTaille() == 10);
	}
	
	@Test
	void getCoordTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.OUEST);
		assertTrue(l.getCoord().equals(new Coordonnée(1,1)));
	}
	
	@Test
	void getDirectionTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.OUEST);
		assertTrue(l.getDirection() == Direction.OUEST);
	}
	
	@Test
	void setDirectionTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.OUEST);
		l.setDirection(Direction.EST);
		assertFalse(l.getDirection() == Direction.OUEST);
		assertTrue(l.getDirection() == Direction.EST);
	}
	
	@Test
	void setCaracTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c);
		l.setCarac("aaaaa");
		assertFalse(l.getCarac().equals("cccccccccc"));
		assertTrue(l.getCarac().equals("aaaaa"));
		assertFalse(l.getTaille() == 10);
		assertTrue(l.getTaille() == 5);
	}
	
	@Test
	void placableHorTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("ccccccccc", c);
		assertTrue(l.placable(20,20));
		assertFalse(l.placable(5, 5));
	}
	
	@Test
	void placableVertTest() {
		Coordonnée c = new Coordonnée(10,10);
		Ligne l = new Ligne("cccccccccc", c, Direction.SUD);
		assertTrue(l.placable(20,20));
		assertFalse(l.placable(5, 5));
	}
	
	@Test
	void placableDiagTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.NORD_EST);
		assertTrue(l.placable(20,20));
		assertFalse(l.placable(20, 5));
		assertFalse(l.placable(5, 20));
	}
	
	@Test
	void déplacerTest() {
		Coordonnée c = new Coordonnée(1,1);
		Coordonnée coord = new Coordonnée(4,4);
		Ligne l = new Ligne("cccccccccc", c, Direction.NORD_EST);
		l.déplacer(coord, 100, 100);
		assertTrue(l.getCoord().equals(coord));
		l.déplacer(c, 100, 100);
		try {
			l.déplacer(coord, 11, 11);
		}catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(),"nouvelle coordonnée invalide");
		}
		assertFalse(l.getCoord().equals(coord));
	}
	
	@Test
	void encadrerTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.NORD_EST);
		l.encadrer('x');
		assertTrue(l.isEncadré());
	}
	
	void desencadrerTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.NORD_EST);
		l.encadrer('x');
		l.desencadrer();
		assertFalse(l.isEncadré());
	}
	
	@Test
	void getBordureTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.NORD_EST);
		l.encadrer('x');
		assertTrue(l.getBordure() == 'x');
	}
	
	//a faire
	@Test
	void dessinerTest() {
		Coordonnée c = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", c, Direction.NORD_EST);
		
	}

}
