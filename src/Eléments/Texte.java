package El�ments;
import java.util.LinkedList;

import Film.InterfaceEl�ment;
import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

public class Texte extends El�ment{
	//Liste de lignes
	private LinkedList<String> lignes;
	
	/**
	 * Constructeur par copie
	 * @param t texte � copier
	 */
	public Texte (Texte t, Coordonn�e coord) {
		super(coord);
		lignes = new LinkedList<String>();
		for(int i = 0; i < t.lignes.size(); ++i) {
			lignes.add(i, new String(t.lignes.get(i)));
		}
	}
	
	/**
	 * Constructeur
	 * @param l Liste de lignes
	 */
	public Texte (LinkedList<String> l, Coordonn�e coord) {
		super(coord);
		lignes = new LinkedList<String>(l);
	}
	
	public Texte (Coordonn�e coord) {
		super(coord);
		lignes = new LinkedList<String>();
	}
	
	/**
	 * Getter pour la liste de ligne du texte
	 * @return LinkedList<String> la liste du texte
	 */
	public LinkedList<String> getLignes(){
		return new LinkedList<String>(lignes);
	}
	
	public void addLigne (String s) {
		lignes.add(s);
	}
	
	/**
	 * Test si deux textes sont �quivalents
	 * @param e texte � comparer
	 * @return True si les texte sont �quivalents  false sinon
	 */
	public boolean equals (InterfaceEl�ment e) {
		if (e == null) {
            return false;
        }
        if (getClass() != e.getClass()) {
            return false;
        }
        final Texte other = (Texte) e;
        if (lignes.size() != other.lignes.size())
        	return false;
        if (!this.getCoord().equals(e.getCoord())){
        	return false;
        }
        for(int i = 0;i < lignes.size();++i) {
        	if (!lignes.get(i).equals(other.lignes.get(i)))
        		return false;
        }
        return true;
	}

	
	/**
	 * Getter pour la ligne du texte
	 * @param index index de la ligne
	 * @return String la ligne
	 */
	public String getLigne (int index) {
		return new String(lignes.get(index));
	}
	
	/**
	 * permet de tester si l'�l�ment peut �tre plac� dans l'image
	 * @param int largeur de l'image
	 * @param int hauteur de l'image
	 * @return true si l'�l�ment peut �tre plac�, false si l'�l�ment ne peux pas �tre plac�
	 */
	public boolean placable(int largeur, int hauteur) {
		if (this.getCoord().getX() < 0 || this.getCoord().getX() > largeur || this.getCoord().getY() < 0 || this.getCoord().getY() > hauteur ) {
			return false;
		}
		else if (this.getCoord().getX() + this.taille()[0] > largeur || this.getCoord().getY() - this.taille()[1] < 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * repr�sentation sous forme de String de l'�l�ment
	 * @return la string repr�sentant l'�l�ment
	 */
	public String toString () {
		StringBuilder s = new StringBuilder();
		for(String i:lignes) {
			s.append(i);
			s.append(System.getProperty("line.separator"));
		}
		return s.toString();
	}
	
	/**
	 * retourne la taille d'un texte
	 * @return un tableau de deux entiers contenants largeur et hauteur
	 */
	public int[] taille() {
		int i=0;
		for(int j = 0; j<lignes.size(); ++j) {
			if (i<= lignes.get(j).length() ) {
				i= lignes.get(j).length();
			}
		}
		int t[] = {i, lignes.size()};
		return t;
	}

	/**
	 * permet de dessiner un l'�l�ment sur une image
	 * @param char[][] image sur lequel on �crit l'�l�ment 
	 */
	@Override
	public void dessiner(char[][] image) {
		for(int i = 0; i < this.lignes.size(); ++i ) {
			for (int j = 0; j< this.lignes.get(i).length();++j) {
				image[this.getCoord().getX() + j][this.getCoord().getY() - i] = this.lignes.get(i).charAt(j);
			}
		}
		if (this.isEncadr�()) {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < taille()[0]+2;++i) {
				sb.append(this.getBordure());
			}
			String s = sb.toString();
			Ligne l1 = new Ligne(s, new Coordonn�e(this.getCoord().getX()-1, this.getCoord().getY()+1));
			Ligne l2 = new Ligne(s, new Coordonn�e(this.getCoord().getX()+taille()[0], this.getCoord().getY()- taille()[1]), Direction.OUEST);
			
			sb = new StringBuilder();
			for(int i = 0; i < taille()[1]+2;++i) {
				sb.append(this.getBordure());
			}
			s = sb.toString();
			Ligne l3 = new Ligne(s, new Coordonn�e(this.getCoord().getX()-1, this.getCoord().getY()+1), Direction.SUD);
			Ligne l4 = new Ligne(s, new Coordonn�e(this.getCoord().getX()+taille()[0], this.getCoord().getY()- taille()[1]), Direction.NORD);
			
			l1.dessiner(image);
			l2.dessiner(image);
			l3.dessiner(image);
			l4.dessiner(image);
		}
	}
}
