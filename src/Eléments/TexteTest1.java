package El�ments;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

class TexteTest1 {

	@Test
	void constructeurTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(1,1);
		Texte t = new Texte(txt, c);
		s1 = "aaaa";
		assertFalse(t.getLigne(0).equals("aaaa"));
		assertTrue(t.getLigne(0).equals("xxxx"));
		txt = new LinkedList<String>();
		assertTrue(t.getLigne(0).equals("xxxx"));
		c.setX(0);
		assertFalse(t.getCoord().equals(c));
		assertFalse(t.isEncadr�());
	}
	
	@Test
	void constructeurCopieTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(1,1);
		Texte t1 = new Texte(txt, c);
		Coordonn�e c2 = new Coordonn�e(2,2);
		Texte t2 = new Texte(t1, c2);
		txt.removeFirst();
		txt.addFirst(s3);
		t1 = new Texte(txt, c);
		assertFalse(t2.getLigne(0).equals("zzzz"));
		assertTrue(t2.getLigne(0).equals("xxxx"));
	}
	
	@Test
	void getLigneTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(1,1);
		Texte t1 = new Texte(txt, c);
		assertTrue(t1.getLigne(0).equals(s1));
		assertTrue(t1.getLigne(1).equals(s2));
		assertTrue(t1.getLigne(2).equals(s3));
	}
	
	@Test
	void getLignesTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(1,1);
		Texte t1 = new Texte(txt, c);
		assertTrue(t1.getLignes().equals(txt));
	}
	
	@Test
	void equalsTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(20,20);
		Texte t1 = new Texte(txt, c);
		Coordonn�e c2 = new Coordonn�e(10,10);
		Texte t2 = new Texte(t1, c);
		assertTrue(t1.equals(t2));
		
		t1.d�placer(c2,100,100);
		assertFalse(t1.equals(t2));
		t1.d�placer(c,100,100);
		
		txt.removeFirst();
		txt.addFirst(s3);
		t1 = new Texte(txt, c);
		assertFalse(t1.equals(t2));
		
		Ligne l = new Ligne("cccccccccc", c);
		assertFalse(t1.equals(l));
	}
	
	@Test
	void encadrerTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(20,20);
		Texte t1 = new Texte(txt, c);
		t1.encadrer('x');
		assertTrue(t1.isEncadr�());
	}
	
	void desencadrerTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(20,20);
		Texte t1 = new Texte(txt, c);
		t1.encadrer('x');
		t1.desencadrer();
		assertFalse(t1.isEncadr�());
	}
	
	@Test
	void getBordureTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(20,20);
		Texte t1 = new Texte(txt, c);
		t1.encadrer('x');
		assertTrue(t1.getBordure() == 'x');
	}
	
	@Test
	void placableTest() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(20,20);
		Texte t1 = new Texte(txt, c);
		assertTrue(t1.placable(100, 100));
		assertFalse(t1.placable(23, 23));
		Coordonn�e c1 = new Coordonn�e(1,1);
		Texte t2 = new Texte(txt, c1);
		assertFalse(t2.placable(100, 100));
		
		
	}
	
	@Test
	void d�placer() {
		String s1 ="xxxx";
		String s2 ="yyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(20,20);
		Texte t = new Texte(txt, c);
		Coordonn�e c1 = new Coordonn�e(1,1);
		try {
			t.d�placer(c1, 100, 100);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "nouvelle coordonn�e invalide");
		}
	}
	
	@Test
	void tailleTest() {
		String s1 ="xxxx";
		String s2 ="yyyyy";
		String s3 ="zzzz";
		LinkedList<String> txt = new LinkedList<String>();
		txt.add(s1);
		txt.addLast(s2);
		txt.addLast(s3);
		Coordonn�e c = new Coordonn�e(20,20);
		Texte t = new Texte(txt, c);
		assertTrue(t.taille()[0] == 5);
		assertTrue(t.taille()[1] == 3);
	}
}
