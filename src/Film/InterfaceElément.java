package Film;

import G�om�trie.Coordonn�e;
import G�om�trie.Direction;

public interface InterfaceEl�ment {

	/**
	 * Test si deux �l�ments sont �quivalents
	 * @param e l'�l�ment � comparer
	 * @return True si les �l�ments sont �quivalents  false sinon
	 */
	public abstract boolean equals(InterfaceEl�ment e);

	public boolean isEncadr�();
	
	public char getBordure();
	
	/**
	 * Retourne la repr�sentation sous forme de String de l'�l�ment
	 * @return la String repr�sentant l'�l�ment
	 */
	public abstract String toString();
	
	/**
	 * permet de dessiner un l'�l�ment sur une image
	 * @param char[][] image sur lequel on �crit l'�l�ment 
	 */
	public abstract void dessiner(char[][] image);
	
	/**
	 * Permet d'encadrer un �l�ment
	 * @param Character c � utiliser pour encadrer
	 */
	public abstract void encadrer(char c);

	/**
	 * Permet de d�sencadrer un �l�ment	
	 */
	public abstract void desencadrer();
	
	/**
	 * Setter pour les coordonn�es de l'�l�ment
	 * @param Coordonn�es c la nouvelle coordonn�e
	 */
	public abstract void d�placer(Coordonn�e c, int largeur, int hauteur) throws IllegalArgumentException;
	
	public abstract void d�placer(int distance, Direction d);
	
	/**
	 * permet de tester si l'�l�ment peut �tre plac� dans l'image
	 * @param int largeur de l'image
	 * @param int hauteur de l'image
	 * @return true si l'�l�ment peut �tre plac�, false si l'�l�ment ne peux pas �tre plac�
	 */
	public abstract boolean placable(int largeur, int hauteur);
	
	/**
	 * Getter pour les coordonn�es de l'�l�ment
	 * @return Coordonn�e de l'�l�ment
	 */
	public abstract Coordonn�e getCoord();

}