package Film;

import java.util.LinkedList;

import El�ments.*;
public class Image {
	//Matrice de caract�re pour le rendu final de l'image

	//Liste d'�l�ments
	private LinkedList<InterfaceEl�ment> �l�ments;
	// Hauteur de l'image
	private final int HAUTEUR;
	//largeur de l'image
	private final int LARGEUR;
	
	/**
	 * Constructeur
	 * @param e liste d'�l�ment � entrer dans l'image
	 * @param hauteur la hauteur de l'image
	 * @param largeur la largeur de l'image
	 */
	public Image (LinkedList<InterfaceEl�ment> e, int hauteur, int largeur) throws IllegalArgumentException {
		if( hauteur <= 0 || largeur <= 0) {
			throw new IllegalArgumentException("Taille invalide");
		}
		�l�ments = new LinkedList<InterfaceEl�ment>(e);
		HAUTEUR = hauteur;
		LARGEUR = largeur;
	}
	
	/**
	 * Constructeur par copie
	 * @param i image � copier
	 */
	public Image(Image i) {
		�l�ments = new LinkedList<InterfaceEl�ment>(i.getEl�ments());
//		for(InterfaceEl�ment ie: i.getEl�ments()) {
//			
//		}
		HAUTEUR = i.getHauteur();
		LARGEUR = i.getLargeur();
	}
	
	/**
	 * Constructeur vide
	 * @param largeur de l'image
	 * @param hauteur de l'image
	 */
	public Image(int largeur, int hauteur) throws IllegalArgumentException{
		if( hauteur <= 0 || largeur <= 0) {
			throw new IllegalArgumentException("Taille invalide");
		}
		�l�ments = new LinkedList<InterfaceEl�ment>();
		HAUTEUR = hauteur;
		LARGEUR = largeur;
	}

	/**
	 * Getter pour la hauteur de l'image
	 * @return int hauteur la largeur de l'image
	 */
	public int getHauteur () {
		return HAUTEUR;
	}
	
	/**
	 * Getter pour la largeur de l'image
	 * @return int LARGEUR la largeur de l'image
	 */
	public int getLargeur () {
		return LARGEUR;
	}
	
	/**
	 * Getter pour la liste des �l�ments de l'image
	 * @return LinkedList<El�ment> liste des �l�ment de l'image
	 */
	public LinkedList<InterfaceEl�ment> getEl�ments () {
		//avant : return �l�ments
		return new LinkedList<InterfaceEl�ment>(�l�ments);
	}
	
	/**
	 * Ajouter un �l�ment sur une layer
	 * @param e �l�ment � ajouter
	 * @param index layer de l'�l�ment
	 * @param c1 position de l'�l�ment
	 */
	public void addElement (InterfaceEl�ment e, int index) {
		/*if (c1.getX() < 0 || c1.getX() > LARGEUR || c1.getY() < 0 || c1.getY() > HAUTEUR ) {
			throw new IllegalArgumentException("Coordon�e invalide");
		}*/
		�l�ments.add(index, e);
		
	}
	
	/**
	 * Ajouter un �l�ment
	 * @param e �l�ment � ajouter
	 * @param c1 coordonn�e o� ajouter l'�l�ment
	 */
	public void addElement (InterfaceEl�ment e) {
		�l�ments.addLast(e);
	}
	
	/**
	 * Supprimer un �l�ment
	 * @param e �l�ment � supprimer
	 */
	public void supprElement (InterfaceEl�ment e) throws IllegalArgumentException{
		if(!�l�ments.contains(e)) {
			throw new IllegalArgumentException("El�ment invalide");
		}
		�l�ments.remove(e);
	}
	
	/**
	 * d�placer un �l�ment � un layer pr�cis
	 * @param e
	 * @param index
	 * @throws IllegalArgumentException si l'�l�ment n'est pas dans l'image
	 */
	public void changeOrdre(InterfaceEl�ment e, int index) throws IllegalArgumentException {
		if (!�l�ments.contains(e)) {
			throw new IllegalArgumentException("L'image ne contient pas l'�l�ment");
		}
		if (index < 0 || index > �l�ments.size()) {
			throw new IllegalArgumentException("Index invalide");
		}
		�l�ments.remove(e);
		�l�ments.add(index, e);
	}
	
	/**
	 * Mettre un �l�ment d�rri�re l'autre
	 * @param e1 �l�ment � d�placer
	 * @param e2 �l�ment derri�re lequel on d�place
	 * @throws IllegalArgumentException si un des �l�ments n'est pas dans l'image
	 */
	public void mettreApr�s (El�ment e1, InterfaceEl�ment e2) throws IllegalArgumentException {
		if (!�l�ments.contains(e1) || !�l�ments.contains(e2)) {
			throw new IllegalArgumentException("L'image ne contient pas l'�l�ment");
		}
		int i = �l�ments.indexOf(e2);
		changeOrdre(e1, i+1);
	}
	
	/**
	 * Mettre un �l�ment devant l'autre
	 * @param e1 �l�ment � d�placer
	 * @param e2 �l�ment devant lequel on d�place
	 * @throws IllegalArgumentException si un des �l�ments n'est pas dans l'image
	 */
	public void mettreAvant (El�ment e1, InterfaceEl�ment e2) throws IllegalArgumentException {
		if (!�l�ments.contains(e1) || !�l�ments.contains(e2)) {
				throw new IllegalArgumentException("L'image ne contient pas l'�l�ment");
		}
		int i = �l�ments.indexOf(e2);
		changeOrdre(e1, i-1);
	}
	
	public String rendre () {
		char[][] image = new char[LARGEUR][HAUTEUR];
		StringBuilder s = new StringBuilder();
		for(InterfaceEl�ment e:�l�ments) {
			e.dessiner(image);
		}
		for(int j =image.length - 1; j >=0 ;--j) {
			for(int k = 0;k < image[j].length;++k) {
				s.append(image[k][j]);
			}
			s.append(System.getProperty("line.separator"));
		}
		return s.toString();
	}
}
