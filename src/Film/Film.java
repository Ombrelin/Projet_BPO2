package Film;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Film {
	//Liste d'image
	private ArrayList<Image> images;
	//Nombre de lignes
	private final int NB_LIGNES;
	//Nomre de colonnes
	private final int NB_COLONNES;
	
	/**
	 * Constructeur
	 * @param i liste d'image
	 * @param nbLignes nombre de lignes
	 * @param nbColonnes nombre de colonnes
	 */
	public Film(ArrayList<Image> i, int nbLignes, int nbColonnes) throws IllegalArgumentException {
		if (nbLignes < 0 || nbColonnes < 0) {
			throw new IllegalArgumentException("Dimensions invalides");
		}
		NB_LIGNES = nbLignes;
		NB_COLONNES = nbColonnes;
		images = new ArrayList<Image>(i);		
		for(Image img:images) {
			if(img.getHauteur() != NB_LIGNES || img.getLargeur() != NB_COLONNES) {
				throw new IllegalArgumentException("Images ou taille invalides");
			}
		}	
	}
	
	/**
	 * Constructeur vide
	 * @param nbLignes nombre de lignes
	 * @param nbColonnes nombre de colonnes
	 */
	public Film( int nbLignes, int nbColonnes) throws IllegalArgumentException {
		if (nbLignes<0 || nbColonnes < 0) {
			throw new IllegalArgumentException("Dimensions invalides");
		}
		images = new ArrayList<Image>();
		NB_LIGNES = nbLignes;
		NB_COLONNES = nbColonnes;
	}
	
	/**
	 * ajoute une image � la fin du film
	 * @param i image � ajouter 
	 */
	public void addImg (Image i) throws IllegalArgumentException {
		if (i.getHauteur() != NB_LIGNES || i.getLargeur() != NB_COLONNES) {
			throw new IllegalArgumentException("Dimensions de l'image invalide");
		}
		images.add(new Image(i));
	}
	
	/**
	 * ajoute une image � un endroit du film
	 * @param i image � ajouter
	 * @param index index auquel ajouter l'image
	 */
	public void addImg(Image i, int index) throws IllegalArgumentException {
		if (i.getHauteur() != NB_LIGNES || i.getLargeur() != NB_COLONNES) {
			throw new IllegalArgumentException("Dimensions de l'image invalide");
		}
		images.add(index,new Image(i));
	}
	
	/**
	 * rend toutes les images d'un film
	 * @return String avec les images du film correctement format�
	 */
	public String rendre () throws IllegalStateException {
		if (images.size() == 0) {
			throw new IllegalStateException("Le film ne contient pas d'images");
		}
		StringBuilder rendu = new StringBuilder();
		rendu.append(NB_LIGNES + " " + NB_COLONNES);
		rendu.append(System.getProperty("line.separator"));
		for(Image i:images) {
			rendu.append("\\newframe");
			rendu.append(i.rendre());
		}
		return rendu.toString();
	}
	
	/**
	 * Cr�er un fichier avec le rendu du film
	 * @param nomFichier nomme le fichier
	 * @param rendu rendu du film
	 * @throws IOException si la cr�ation du fichier �choue
	 */
	public void imprimer (String nomFichier, String rendu) throws IOException {
		PrintWriter out;
		try {
			out = new PrintWriter(nomFichier + ".txt");
		} catch (Exception e) {
			throw new IOException("Erreur � la cr�ation du fichier");
		}
		out.print(rendu);
		out.close();
	}
}