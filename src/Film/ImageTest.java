package Film;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

import Eléments.Caractère;
import Eléments.Ligne;
import Géomètrie.Coordonnée;

class ImageTest {

	@Test
	void constructeurTest() {
		Caractère c = new Caractère('a', new Coordonnée(1,1));
		Coordonnée coord = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", coord);
		LinkedList<InterfaceElément> liste = new LinkedList<InterfaceElément>();
		Image i = new Image(liste, 100, 100);
		assertTrue(i.getHauteur() == 100);
		assertTrue(i.getLargeur() == 100);
		assertTrue(i.getEléments().equals(liste));
	}
	
	@Test
	void constructeurParCopieTest() {
		Caractère c = new Caractère('a', new Coordonnée(1,1));
		Coordonnée coord = new Coordonnée(1,1);
		Ligne l = new Ligne("cccccccccc", coord);
		LinkedList<InterfaceElément> liste = new LinkedList<InterfaceElément>();
		Image i1 = new Image(liste, 100, 100);
		Image i2 = new Image(i1);
		//assertTrue(i1.equals(i2));
		assertFalse();
	}

}
