import java.util.LinkedList;

import Eléments.*;
import Film.*;
import Géomètrie.*;

public class Appli {
	public static void main(String[] args) {
		
		/* Film f = new Film(20, 20);
		Caractère c1 = new Caractère('a', new Coordonnée(0,0));
		Ligne l1 = new Ligne("Bonjour", new Coordonnée(5,10), Direction.NORD);
		Image i = new Image(20,20);
		char[][] c = new char[20][20];
		//l1.dessiner(c);
		LinkedList<String> l = new LinkedList<String>();
		l.add("bonjour");
		l.add("aurevoir");
		l.add("hélicoptère");
		Texte t = new Texte(l, new Coordonnée(5,10));
		Extrait e = new Extrait(t,new Coordonnée(0,1), new Coordonnée(10,0), new Coordonnée(5,14));
		t.dessiner(c);
		e.dessiner(c);
		c1.encadrer(new Character('x'));
		c1.dessiner(c);
		StringBuilder sb = new StringBuilder();
		for(int j =c.length - 1; j >=0 ;--j) {
			for(int k = 0;k < c[j].length;++k) {
				sb.append(c[k][j]);
			}
			sb.append("\n");
		}
		String s = sb.toString();
		System.out.println(s);
		i.addElement(l1);
		
		i.addElement(c1);
		f.addImg(i);
		l1.setDirection(Direction.SUD_EST);
		f.addImg(i);
		System.out.println(f.rendre());
		*/
		
		Film f = new Film(100, 100);
		Image i = new Image(100,100);
		//Caractère c = new Caractère('a', new Coordonnée(5,5));
		LinkedList<String> l = new LinkedList<String>();
		l.add("bonjour");
		l.add("aurevoir");
		l.add("hélicoptère");
		Texte t = new Texte(l, new Coordonnée(5,10));
		Extrait e = new Extrait(t,new Coordonnée(0,1), new Coordonnée(10,0), new Coordonnée(5,14));
		Ligne ligne = new Ligne("Hélicoptère", new Coordonnée(5,14));
		Ligne ligne1 = new Ligne("Hélicoptère", new Coordonnée(20,14));
		ligne1.setDirection(Direction.SUD_OUEST);
		ligne.setDirection(Direction.SUD_EST);
		Ligne l1 = new Ligne("OOOO", new Coordonnée(31,14));
		Ligne l4 = new Ligne("OOOO", new Coordonnée(31,14));
		Ligne l3 = new Ligne(l1);
		Ligne l2 = new Ligne(l3);
		Ligne l5 = new Ligne("OOOOO", new Coordonnée(31,17),Direction.OUEST);
		Ligne l6 = new Ligne("OOOO", new Coordonnée(31,11),Direction.EST);
		Ligne l7 = new Ligne("OOOO", new Coordonnée(27,14),Direction.SUD);
		Ligne l9 = new Ligne("OOOO", new Coordonnée(35,14),Direction.NORD);
		t.encadrer('*');
		Ligne testEncadrer = new Ligne("xxxxxxxxxxxxx", new Coordonnée(15,30), Direction.SUD);
		testEncadrer.encadrer('#');
		//i.addElement(testEncadrer);
		Caractère testMvDir = new Caractère('#', new Coordonnée(5,10));
		testMvDir.déplacer(1,Direction.SUD_EST);
		//i.addElement(testMvDir);
		/*i.addElement(l9);
		i.addElement(l6);
		i.addElement(l5);
		i.addElement(l7);
		l4.setDirection(Direction.SUD);
		l3.setDirection(Direction.NORD);
		l2.setDirection(Direction.OUEST);
		i.addElement(l4);
		i.addElement(l3);
		i.addElement(l2);
		i.addElement(l1);*/
		//i.addElement(t);
		Extrait ex = new Extrait(t,new Coordonnée(0,1), new Coordonnée(10,0), new Coordonnée(5,14));
		ex.encadrer('x');
		
		LinkedList<InterfaceElément> liste = new LinkedList<InterfaceElément>();
		liste.add(ex);
		liste.add(testEncadrer);
		Coordonnée gcoord = new Coordonnée(10, 99);
		Groupe g = new Groupe(liste, gcoord);
		i.addElement(g);
		gcoord.setX(50);
		g.déplacer(gcoord, 100, 100);
		//i.addElement(g);
		//i.addElement(e);
		//i.addElement(ligne);
		//i.addElement(ligne1);
		f.addImg(i);
		System.out.println(f.rendre());
	}
}
